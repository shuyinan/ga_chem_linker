#include "base.h"

base::base() {
  value=-1;
  for (int ibase=0; ibase < MAX_SITES_IN_FRAG; ibase++) {
    next_bases[ibase] = NULL;
  }
  fragments=NULL;
}

base::base(int val, fragment** frags) {
  value=val;
  for (int ibase=0; ibase < MAX_SITES_IN_FRAG; ibase++) {
    next_bases[ibase] = NULL;
  }
  fragments=frags;
}

base::~base() {
  for (int inext=0; inext<fragments[value]->get_num_sites(); inext++) {
    delete next_bases[inext];
  }
  return;
}

base* base::get_next_bases(int index) {
  return next_bases[index];
}

void base::set_next_bases(base* next, int index) {
  next_bases[index] = next;
}

int base::get_value() {
  return value;
}

void base::set_value(int val) {
  int num_sites_old = fragments[value]->get_num_sites();
  int i_allow_frag[MAX_ALLOWED_FRAGS];
  value = val;
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    //printf("sv %i\n", isite);
    int nallow = fragments[value]->get_num_allowed(isite);
    fragments[value]->get_allowed_fragments(isite, i_allow_frag);
    int iallow=0;
    for (int ifrag=0; ifrag<nallow; ifrag++) {
      if (next_bases[isite] != NULL) {
	if (next_bases[isite]->get_value() == i_allow_frag[ifrag]) iallow=1;
      }
    }
    if (!iallow) {
      if (next_bases[isite]!=NULL) {
	delete next_bases[isite];
      }
      next_bases[isite] = new base(i_allow_frag[0], fragments);
    }
  }
  for (int isite=fragments[value]->get_num_sites(); isite < num_sites_old;
       isite++) {
    delete next_bases[isite];
    next_bases[isite]=NULL;
  }
}

void base::initialize_random_children(int* base_index) {
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    next_bases[isite] = new base(base_index[isite+1], fragments);
    next_bases[isite]->initialize_random_children(base_index);
  }
}

int base::count_me(int num) {
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    num = next_bases[isite]->count_me(num);
  }
  num++;
  return num;
}

int base::print_me() {
  printf("[%i", value);
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    next_bases[isite]->print_me();
  }
  printf("]");
}

int base::print_me_restart_total() {
  FILE* check = fopen("check.dat", "a+");
  fprintf(check, "%i ", fragments[value]->get_num_sites()+1);
  fclose(check);
}

int base::print_me_restart() {
  FILE* check = fopen("check.dat", "a+");
  fprintf(check, "%i ", value);
  fclose(check);
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    next_bases[isite]->print_me_restart();
  }
}

// this is not done yet, must clean up memory
int base::mutate_me(double mutprob, int nallow, int* i_allow_frag) {
  double random_number = (double)rand() / (double)RAND_MAX;
  if (random_number < mutprob  && nallow  > 1) {
    //int num_sites_old = fragments[value]->get_num_sites();
    printf(" mutation allowed");
    int irand = rand() % nallow;
    int tmpvalue = i_allow_frag[irand];
    printf(" --> %i|", tmpvalue);
    set_value(tmpvalue);
    //printf("mm mutated\n");
  }
  else
  printf(" NA|");
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    printf(" %i", isite);
    nallow = fragments[value]->get_num_allowed(isite);
    //printf("mm b %i\n", isite);
    fragments[value]->get_allowed_fragments(isite, i_allow_frag);
    //printf("mm c %i\n", isite);
    next_bases[isite]->mutate_me(mutprob, nallow, i_allow_frag);
    //printf("mm d %i\n", isite);
  }
}
  
int base::get_my_base(int* bases, int num) {
  bases[num] = value;
  //printf("gmb %i\n",value);
  num++;
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    num = next_bases[isite]->get_my_base(bases, num);
  }
  return num;
}
  
int base::add_my_bases(int* bases, int num) {
  //printf("amb aa %i %i %i\n",num, value, fragments);
  num++;
  //printf("amb a %i %i %i\n",num, value, fragments[value]->get_num_sites());
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    //printf("amb b %i %i\n",isite, bases[num]);
    next_bases[isite]=new base(bases[num], fragments);
    //printf("amb c %i\n",isite);
    num = next_bases[isite]->add_my_bases(bases, num);
    //printf("amb d %i\n",isite);
  }
  return num;
}

int base::getting_base_by_index(int ibase, int num, base** base_out) {
  num++;
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    if (num==ibase) {
      *base_out = next_bases[isite];
    }
    num = next_bases[isite]->getting_base_by_index(ibase, num, base_out);
  }
  return num;
}

int base::replacing_base_by_index(int ibase, int num, base* base_in) {
  num++;
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    if (num==ibase) {
      next_bases[isite] = base_in;
    }
    num = next_bases[isite]->replacing_base_by_index(ibase, num, base_in);
  }
  return num;
}

int base::checking_allowed_base_by_index(int ibase, int num, base* base_in, 
					 int* ret) {
  num++;
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    if (num==ibase) {
      *ret=allows_base_at_site(base_in, isite);
      //printf("cabbi %i %i\n", num, *ret);
    }
    num = next_bases[isite]->checking_allowed_base_by_index(ibase, num, 
							    base_in, ret);
  }
  return num;
}

int base::allows_base_at_site(base* base_in, int isite) {
  int nallow=fragments[value]->get_num_allowed(isite);
  if (isite >= nallow) return 0;
  int tmp[nallow];
  fragments[value]->get_allowed_fragments(isite, tmp);
  for (int iallow=0; iallow < nallow; iallow++) {
    if (base_in->get_value() == tmp[iallow]) return 1;
  }
  return 0;
}
	 
int base::write_my_zmat(FILE* zmat, int iatom, int isite_dist, int isite_ang, int isite_dihed, double site_dihed, int ioffset) {
  int idist, iang, idihed, jatom;
  double dist, ang, dihed;
  //printf("in wmz %i %i %i %i %f\n", iatom, isite_dist, isite_ang, isite_dihed, site_dihed);
  int ioffsetsites=iatom;
  for (jatom=0; jatom<fragments[value]->get_num_atoms(); jatom++) {
    if (jatom==0) {
      idist=isite_dist+1+ioffset;
      iang=isite_ang+1+ioffset;
      idihed=isite_dihed+1+ioffset;
      dist=fragments[value]->get_distance(jatom);
      ang=fragments[value]->get_angle(jatom);
      dihed=site_dihed;
    } else if (jatom==1) {
      idist=fragments[value]->get_idistance(jatom)+1+ioffsetsites;
      iang=isite_dist+1+ioffset;
      idihed=isite_ang+1+ioffset;
      dist=fragments[value]->get_distance(jatom);
      ang=fragments[value]->get_angle(jatom);
      dihed=fragments[value]->get_dihedral(jatom);
    } else if (jatom==2) {
      idist=fragments[value]->get_idistance(jatom)+1+ioffsetsites;
      iang=fragments[value]->get_iangle(jatom)+1+ioffsetsites;
      idihed=isite_dist+1+ioffset;
      dist=fragments[value]->get_distance(jatom);
      ang=fragments[value]->get_angle(jatom);
      dihed=fragments[value]->get_dihedral(jatom);
    } else {
      idist=fragments[value]->get_idistance(jatom)+1+ioffsetsites;
      iang=fragments[value]->get_iangle(jatom)+1+ioffsetsites;
      idihed=fragments[value]->get_idihedral(jatom)+1+ioffsetsites;
      dist=fragments[value]->get_distance(jatom);
      ang=fragments[value]->get_angle(jatom);
      dihed=fragments[value]->get_dihedral(jatom);
    }
    if (iatom==0) {
      fprintf(zmat,"%s \n", fragments[value]->get_label(jatom));
    } else if (iatom==1) {
      fprintf(zmat,"%s  %i  %lf\n", fragments[value]->get_label(jatom),
	      idist, dist);
    } else if (iatom==2) {
      fprintf(zmat,"%s  %i  %lf  %i  %lf  \n", 
	      fragments[value]->get_label(jatom),
	      idist, dist, iang, ang);
    } else {
      fprintf(zmat,"%s  %i  %lf  %i  %lf  %i  %lf\n", 
	      fragments[value]->get_label(jatom),
	      idist, dist, iang, ang, idihed, dihed);
    }
    iatom++;
  }
  for (int isite=0; isite<fragments[value]->get_num_sites(); isite++) {
    if (jatom==1) {
      iatom=next_bases[isite]->write_my_zmat(zmat, iatom, 
					     fragments[value]->get_isite_distance(isite), 
					     isite_dist, 
					     isite_ang, 
					     fragments[value]->get_site_dihedral(isite),
					     ioffsetsites);
    } else if (jatom==2) {
      iatom=next_bases[isite]->write_my_zmat(zmat, iatom, 
					     fragments[value]->get_isite_distance(isite), 
					     fragments[value]->get_isite_angle(isite), 
					     isite_dist, 
					     fragments[value]->get_site_dihedral(isite),
					     ioffsetsites);
    } else {
      /*printf("before wmz %i %i %i %f\n",
	     fragments[value]->get_isite_distance(isite), 
	     fragments[value]->get_isite_angle(isite), 
	     fragments[value]->get_isite_dihedral(isite), 
	     fragments[value]->get_site_dihedral(isite));*/
      iatom=next_bases[isite]->write_my_zmat(zmat, iatom, 
					     fragments[value]->get_isite_distance(isite), 
					     fragments[value]->get_isite_angle(isite), 
					     fragments[value]->get_isite_dihedral(isite), 
					     fragments[value]->get_site_dihedral(isite),
					     ioffsetsites);
      
    }
  }
  return iatom;
}
