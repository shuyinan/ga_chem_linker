#ifndef _pool_h_
#define _pool_h_

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "gene.h"
#include "genelist.h"
#include "fragment.h"

class pool
{
 public:
  pool();
  pool(int ngenes, int ncrosses, double mutprob, fragment** frags, int fnav, int* nav);
  ~pool();
  void crossover();
  void initialize_random(int igene, int* base);
  void read_restart(genelist* db, fragment** frags);
  void initialize_zmatrix(int igene);
  void initialize_fitness(genelist* db, int size);
  void get_fitness_mpi(genelist* db, int size, int num_genes_c);
  //void set_initial_value(int fnav, int* nav);
  void print_pool(int igene);
  void print_generation(genelist* db);
  //void print_crosses();
  void print_selects();
  void print_after_mutation();
  void select(genelist* db);
  void pick_fittest(genelist* db, int size);
  void mutate(int fnav, int* fav);
  int get_calculation_list (gene* gene_in, genelist* db, int num_genes_c);
  double check_fitness(gene* gene_in, genelist* db);
  double calculate_fitness(int gene_id);
  int contains_gene(gene* gene_in);
  void print_restart(genelist* db);

 private:
  int num_genes;
  int num_crosses;
  gene** genes;
  //gene** crosses;
  gene** selects;
  gene** calculates;
  //int num_bases;
  fragment** fragments;
  //  int* max_values;
  //  double mutation_probability;
};

#endif
