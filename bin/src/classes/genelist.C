#include "genelist.h"

genelist::genelist() {
  first=NULL;
  length=0;
}

genelist::~genelist() {
  genenode* tmp;
  genenode* tmp2;
  tmp = first;
  while (tmp !=NULL) {
    tmp2=tmp;
    tmp=tmp->get_next();
    delete tmp2;
  }
}

int genelist::add(gene* gene_in, double fit_in) {
  //printf("add a %i\n", gene_in->get_fragments());
  int nbases=gene_in->get_num_bases();
  //printf("add b\n");
  //int tmp[nbases];
  //gene_in->get_max_values(tmp);
  double mutprob=gene_in->get_mutation_probability();
  //printf("add c\n");
  genenode* new_node = new genenode(gene_in, fit_in,mutprob);
  //printf("add d %i\n", new_node->get_gene()->get_fragments());

  printf("Gene added into database.\n");
  //gene_in->print_gene();
  //printf("with fitness %f into database\n", new_node->get_fitness());

  if (first==NULL) {
    first=new_node;
    length=1;
    new_node->set_next(NULL);
    return length;
  }
  if (new_node->is_less_than(first->get_gene())) {
    new_node->set_next(first);
    first=new_node;
    length++;
    return length;
  }
  genenode* current = first;
  while (new_node->is_greater_than(current->get_gene()) && 
	 current->get_next() != NULL) {
    if (new_node->is_less_than(current->get_next()->get_gene())) {
      new_node->set_next(current->get_next());
      current->set_next(new_node);
      length++;
      return length;
    }
    current=current->get_next();
  }
  /*if (new_node->is_greater_than(current->get_gene())) {
    new_node->set_next(current->get_next());
    current->set_next(new_node);
    length++;
    } */
  if (new_node->is_greater_than(current->get_gene())) {
    current->set_next(new_node);
    new_node->set_next(NULL);
    length++;
  }
  return length;
}

int genelist::find_gene(gene* gene_in, double* fit_out) {
  if (first==NULL) return 0;
  genenode* current = first;
  while (current->is_less_than(gene_in) && current->get_next() != NULL) {
    current=current->get_next();
  }
  if (current->is_equal_to(gene_in)) {
    *fit_out=current->get_fitness();
    printf("Found fitness of %f for gene ",*fit_out);
    gene_in->print_gene();
    printf("\n");
    return 1;
  }
  return 0;
}

void genelist::write_to_disk() {
  FILE* dbfile = fopen("MolGA.db", "w");

  fprintf(dbfile, "%i\n",length);
  
  genenode* current=first;

  int* tmp;

  for (int irec=0; irec < length; irec++) {
    int nbases = current->get_gene()->get_num_bases();
    tmp = (int*)malloc(nbases*sizeof(int));
    current->get_gene()->get_bases(tmp);
    fprintf(dbfile, "%f ", current->get_fitness());
    fprintf(dbfile, "%i ",nbases);    
    for (int ibase=0; ibase < nbases; ibase++) {
      fprintf(dbfile, "%i ",tmp[ibase]);
    }
    fprintf(dbfile, "\n");
    free(tmp);
    current=current->get_next();
  }

  fclose(dbfile);
}

void genelist::read_from_disk(fragment** frags) {
  FILE* dbfile;
  int len;

  if (dbfile =fopen("MolGA.db", "r")) {
    printf("Searching database file MolGA.db\n");
    
    fscanf(dbfile, "%i",&len);
    
    printf("Database has %i genes\n", len);
    
    genenode* current=first;
    
    int nbases;
    
    int* tmp;
    double fit;
    
    for (int irec=0; irec < len; irec++) {
      fscanf(dbfile, "%lf ", &fit);
      
      printf("rfd %f\n", fit);
      
      fscanf(dbfile, "%i ",&nbases);
      
      printf("rfd %i\n", nbases);
      
      tmp=(int*)malloc(nbases*sizeof(int));
      
      for (int ibase=0; ibase < nbases; ibase++) {
	printf("before fscan\n");
	fscanf(dbfile, "%i ",&(tmp[ibase]));
	printf("after fscan %i %i \n", ibase, tmp[ibase]);
      }
      
      gene* new_gene = new gene(0.0, frags, 0, NULL);
      
      new_gene->set_bases(tmp);
      
      add(new_gene, fit);    
      
      new_gene=NULL;
      
      free(tmp);
    }

    fclose(dbfile);
  }
}

int genelist::get_length() {
  return length;
}
