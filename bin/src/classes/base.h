#ifndef _base_h_
#define _base_h_

#include <stdlib.h>
#include <stdio.h>

#include "fragment.h"

class base
{
 public:
  base();
  base(int val, fragment** frags);
  ~base();
  base* get_next_bases(int index);
  void set_next_bases(base* next, int index);
  int get_value();
  void set_value(int val);
  void initialize_random_children(int* base_index);
  int count_me(int num);
  int print_me();
  int print_me_restart_total();
  int print_me_restart();
  int mutate_me(double mutprob, int nallow, int* i_allow_frag);
  int get_my_base(int* bases, int num);
  int add_my_bases(int* bases, int num);
  int replacing_base_by_index(int ibase, int num, base* bases_in);
  int getting_base_by_index(int ibase, int num, base** bases_out);
  int checking_allowed_base_by_index(int ibase, int num, base* bases_out, 
				     int* ret);
  int allows_base_at_site(base* ibase, int isite);
  int write_my_zmat(FILE* zmat, int iatom, int isite_dist, int isite_ang, int isite_dihed, double site_dihed, int ioffset);

 private:
  int value;
  base* next_bases[MAX_SITES_IN_FRAG];
  fragment** fragments;
};

#endif
