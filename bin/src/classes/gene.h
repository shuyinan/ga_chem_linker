#ifndef _gene_h_
#define _gene_h_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "base.h"
#include "fragment.h"

class gene
{
 public:
  gene();
  gene(fragment** frag);
  gene(double mutprob, fragment** frag, int fnav, int* nav);
  ~gene();
  void get_bases(int* bases);
  void set_bases(int* bases);
  void copy(gene* to_be_copied);
  void mutate(int fnav, int* fav);
  void initialize_random(int val, int* base_index);
  void print_gene();
  void print_gene_restart();
  int get_num_bases();
  fragment** get_fragments();
  double get_mutation_probability();
  void set_mutation_probability(double mutprob);
  void set_first_allowed_values(int first_nallow, int* first_allow);
  int get_first_num_allowed_values();
  void get_first_allowed_values(int* first_allow);
  base* get_base_by_index(int ibase);
  void replace_base_by_index(int ibase, base* base_in);
  int allows_base_by_index(int ibase, base* base_in);
  void crossover_with(gene* gene2);
  int is_less_than(gene* gene2);
  int is_greater_than(gene* gene2);
  int is_equal_to(gene* gene2);
  void write_zmat(int ng);

 private:
  base* first_base;
  double mutation_probability;
  fragment** fragments;
  int first_num_allowed_values;
  int first_allowed_values[MAX_ALLOWED_FRAGS];
};

#endif
