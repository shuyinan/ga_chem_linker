#include "pool.h"
#include "mpi.h"

pool::pool() {
  num_genes=0;
  num_crosses=0;
  genes=NULL;
  //crosses=NULL;
  //num_bases=0;
  fragments=NULL;
}

pool::pool(int ngenes, int ncrosses, double mutprob, fragment** frags, int fnav, int* nav) {
  num_genes=ngenes;
  num_crosses=ncrosses;
  genes = (gene**)malloc(num_genes*sizeof(gene*));
  //crosses = (gene**)malloc(num_genes*sizeof(gene*));
  selects = (gene**)malloc(num_genes*sizeof(gene*));
  calculates = (gene**)malloc(2*num_genes*sizeof(gene*));
  for (int igene = 0; igene < num_genes; igene++) {
    genes[igene] = new gene(mutprob, frags, fnav, nav);
    //crosses[igene] = new gene(mutprob, frags);
    selects[igene] = new gene(mutprob, frags, fnav, nav);
  }
  for (int igene = 0; igene < 2*num_genes; igene++) {
  calculates[igene] = new gene(mutprob, frags, fnav, nav);
  }
  fragments=frags;
}

pool::~pool() {
  for (int igene = 0; igene < num_genes; igene++) {
    if (genes[igene] != NULL ) {
      delete genes[igene];
      delete selects[igene];
    }
  }
  free(genes);
}

void pool::crossover() {
  printf(" \n");
  printf("-----Starting Crossover-----\n");
  
  int list1[num_genes];
  int list2[num_genes];
  for (int igene = 0; igene < num_genes; igene++) {
    list1[igene] = igene;
  }
  
  //printf("list1 %i %i\n",list1[0], list1[1]);

  for (int igene = 0; igene < 2*num_crosses; igene++) {
    int i = rand() % (num_genes - igene);
    //printf("igene, i %i %i\n",igene,i);
    //printf("%i\n", list1[i]);
    list2[igene] = list1[i];
    //printf("%i\n", list1[num_genes - igene - 1]);
    list1[i] = list1[num_genes - igene - 1];
  }
  
  //printf("list1 %i %i\n",list1[0], list1[1]);
  //printf("list2 %i %i\n",list2[0], list2[1]);

  for (int ipair = 0; ipair < 2*num_crosses; ipair+=2) {
    printf("Selected pair of genes: %i, %i\n", list2[ipair], list2[ipair+1]);
    selects[list2[ipair]]->crossover_with(selects[list2[ipair+1]]);
    /*int i = 1 + rand() % (num_bases-1);
    printf("i %i\n", i);
    int tmp1[num_bases];
    int tmp2[num_bases];
    selects[list2[ipair]]->get_bases(tmp1);
    selects[list2[ipair+1]]->get_bases(tmp2);
    printf("%i %i %i %i %i\n", tmp1[0], tmp1[1], tmp1[2], tmp1[3], tmp1[4]);
    printf("%i %i %i %i %i\n", tmp2[0], tmp2[1], tmp2[2], tmp2[3], tmp2[4]);
    for (int ibase = i; ibase < num_bases; ibase++) {
      int tmp = tmp2[ibase];
      tmp2[ibase] = tmp1[ibase];
      tmp1[ibase] = tmp;
    }
    printf("%i %i %i %i %i\n", tmp1[0], tmp1[1], tmp1[2], tmp1[3], tmp1[4]);
    printf("%i %i %i %i %i\n", tmp2[0], tmp2[1], tmp2[2], tmp2[3], tmp2[4]);
    crosses[ipair]->set_bases(tmp1);
    crosses[ipair+1]->set_bases(tmp2);
    printf("after set_bases\n");*/
  }
  /*for (int ipair = 2*num_crosses; ipair < num_genes; ipair++) {
    crosses[ipair]->copy(selects[list1[ipair-2*num_crosses]]);
    }*/
 printf("-----Done with Crossover-----\n");
 printf(" \n");
}

void pool::initialize_random(int igene, int* base_index) {
    int num_initial_base = 1;
    int initial_base = base_index[0];
    genes[igene]->initialize_random(initial_base, base_index);
}

void pool::read_restart(genelist* db, fragment** frags) {
  double fit;
  int nbases;
  int* tmp;
  FILE* restart = fopen("check.dat" ,"r");
  for (int igene = 0; igene < num_genes; igene++) {
//  char space;
  fscanf(restart, "%lf", &fit);
//  printf("%lf  ", fit);
  fscanf(restart, "%i ",&nbases);
  tmp=(int*)malloc(nbases*sizeof(int));
  for (int ibase=0; ibase < nbases; ibase++) {
    fscanf(restart, "%i ",&(tmp[ibase]));
    }
  genes[igene]->set_bases(tmp);
  printf("Found Gene: ");
  genes[igene]->print_gene();
  printf("in the restart file with fitness: %lf \n", fit);
  db->add(genes[igene], fit);
//  genes[igene]->print_gene();
  free(tmp);
  }
  fclose(restart);
}

void pool::initialize_zmatrix(int igene){
  genes[igene]->write_zmat(igene);
}

void pool::initialize_fitness(genelist* db, int size) {
  int num_genes_c = 0;
  for (int igene = 0; igene < num_genes; igene++) {
  num_genes_c = get_calculation_list(genes[igene], db, num_genes_c);
  }
  get_fitness_mpi(db, size, num_genes_c);
}

void pool::get_fitness_mpi(genelist* db, int size, int num_genes_c){
  int num_gene_eachcore, num_gene_eachcore_add, num_gene_left;
  int gene_id_f, gene_id_i;

  num_gene_eachcore = num_genes_c / size;
  printf("----------Parallelize Information----------\n");
  printf("num_gene_eachcore = %i\n", num_gene_eachcore);
  num_gene_left = num_genes_c % size;
  printf("num_gene_left = %i\n", num_gene_left);
  double calculates_fitness[num_genes_c];
/*write zmat for all the genes*/
for (int igene = 0; igene < num_genes_c; igene++) {
  calculates[igene]->write_zmat(igene);
}

/*distribute calculations to slave processors*/
  for (int irank = 1; irank < size; irank++) {
  int rank_id = irank - 1;
  int rank_id_add = rank_id + 1;
  num_gene_eachcore_add = num_gene_eachcore + 1;
     if (rank_id < num_gene_left) {
     gene_id_f = rank_id_add * num_gene_eachcore_add + num_gene_eachcore;
     gene_id_i = rank_id * num_gene_eachcore_add + num_gene_eachcore;
//     printf("for core %i, id_f = %i, id_i = %i\n", rank_id_add, gene_id_f, gene_id_i);
     printf("number of genes on core %i: %i\n", rank_id_add, num_gene_eachcore_add);
       for (int i = gene_id_i; i < gene_id_f ; i++) {
       MPI_Send (&i,1,MPI_INT,rank_id_add,1,MPI_COMM_WORLD);
//       printf("Send message %i to slave processors %i successfully\n", i, rank_id_add);
       }
     }
     else {
     printf("number of genes on core %i: %i\n", rank_id_add, num_gene_eachcore);
     gene_id_f = rank_id_add * num_gene_eachcore + num_gene_left + num_gene_eachcore;
     gene_id_i = rank_id * num_gene_eachcore + num_gene_left + num_gene_eachcore;
//     printf("for core %i, id_f = %i, id_i = %i\n", rank_id_add, gene_id_f, gene_id_i);
       for (int i = gene_id_i; i < gene_id_f ; i++) {
       MPI_Send (&i,1,MPI_INT,rank_id_add,1,MPI_COMM_WORLD);
//       printf("Send message %i to slave processors %i successfully\n", i, rank_id_add);
       }
     }
  }
// printf("distributuion finished.\n");

/*calculation distributed to master processor*/

  printf("number of genes on core 0: %i\n", num_gene_eachcore);  
  for (int igene = 0; igene < num_gene_eachcore; igene++) {
  calculates_fitness[igene] = calculate_fitness(igene);
  }

/*receive the fitness and print genes*/
  MPI_Status status;
  for (int irank = 1; irank < size; irank++) {
  int igene;
  int rank_id = irank - 1;
  int rank_id_add = rank_id + 1;
  num_gene_eachcore_add = num_gene_eachcore + 1;
     if (rank_id < num_gene_left) {
     gene_id_f = rank_id_add * num_gene_eachcore_add + num_gene_eachcore;
     gene_id_i = rank_id * num_gene_eachcore_add + num_gene_eachcore;
       for (int i = gene_id_i; i < gene_id_f ; i++) {
       MPI_Recv(&igene,1,MPI_INT,rank_id_add,2,MPI_COMM_WORLD,&status);
       MPI_Recv(&calculates_fitness[igene],1,MPI_DOUBLE,rank_id_add,3,MPI_COMM_WORLD,&status);
       printf("Gene with ID: %i, fitness: %lf received from core %i successfully\n", igene, calculates_fitness[igene], rank_id_add);
       }
     }
     else {
     gene_id_f = rank_id_add * num_gene_eachcore + num_gene_left + num_gene_eachcore;
     gene_id_i = rank_id * num_gene_eachcore + num_gene_left + num_gene_eachcore;
       for (int i = gene_id_i; i < gene_id_f ; i++) {
              MPI_Recv(&igene,1,MPI_INT,rank_id_add,2,MPI_COMM_WORLD,&status);
       MPI_Recv(&calculates_fitness[igene],1,MPI_DOUBLE,rank_id_add,3,MPI_COMM_WORLD,&status);
       printf("Gene wih ID: %i, fitness: %lf received from core %i successfully\n", igene, calculates_fitness[igene], rank_id_add);
       }
     }     
  }

  printf("------Done with parallel distribution------\n");

  for (int igene = 0; igene < num_genes_c; igene++) {
  printf("Calculation Done; Fitness for gene ");
  calculates[igene]->print_gene();
  printf("is: %f; ", calculates[igene]);
  db->add(calculates[igene], calculates_fitness[igene]);
  } 
}

void pool::print_pool(int igene) {
    printf("%i.",igene);
    genes[igene]->print_gene();
    printf("\n");
}

void pool::print_generation(genelist* db) {
   printf("Writing generation information to generation.dat \n");
   for (int igene = 0; igene < num_genes; igene++) {
    double fitness=check_fitness(genes[igene], db);
    FILE* generation = fopen("generation.dat", "a+");
    fprintf(generation,  "%lf  ",fitness);
    fclose(generation); 
   }
   FILE* generation = fopen("generation.dat", "a+");
   fprintf(generation,  " \n ");
   fclose(generation); 
   printf("Done with writing generation infomration\n");
   printf(" \n");
}

  
/*void pool::print_crosses() {
  for (int igene = 0; igene < num_genes; igene++) {
    crosses[igene]->print_gene();
    printf(" ");
  }
  printf("\n");
  }*/

void pool::print_selects() {
  printf("Genes Selected for Crossover and Mutation:\n");
  for (int igene = 0; igene < num_genes; igene++) {
    printf("%i.",igene);
    selects[igene]->print_gene();
    printf("\n");
  }
}

void pool::print_after_mutation() {
  printf("Genes after mutation:\n");
  for (int igene = 0; igene < num_genes; igene++) {
    printf("%i.",igene);
    selects[igene]->print_gene();
    printf("\n");
  }
  //printf("\n-----Done with Mutation-----\n");
  printf("\n");
}

void pool::select(genelist* db) {
  printf("-----Starting Selection-----\n");
  for (int iselect = 0; iselect < num_genes; iselect++) {
    int selected=0;
    int icareful=0;
    while (selected==0 && icareful<100000) {
      int itrial = rand() % num_genes;
      double trial_fitness=check_fitness(genes[itrial], db);
      double p = (double)rand() / (double)RAND_MAX;
      if (p < trial_fitness) {
	selects[iselect]->copy(genes[itrial]);
	selected=1;
      }
      icareful++;
    }
  }
 printf("-----Done Selection-----\n");
}
  
void pool::pick_fittest(genelist* db, int size) {
  printf("-----Starting Picking the fiitest genes-----\n");
  double gene_fitness[num_genes];
  double select_fitness[num_genes];
  int num_genes_c = 0;
  for (int igene = 0; igene < num_genes; igene++) {
    num_genes_c = get_calculation_list(genes[igene], db, num_genes_c);
    num_genes_c = get_calculation_list(selects[igene], db, num_genes_c);
  }
  get_fitness_mpi(db, size, num_genes_c);
  for (int igene = 0; igene < num_genes; igene++) {
    gene_fitness[igene] = check_fitness(genes[igene], db);
    select_fitness[igene] = check_fitness(selects[igene], db);
  }
  
  
  for (int iselect = 0; iselect < num_genes; iselect++) {
    double least_fit_gene_fitness;
    int least_fit_gene=-1;
    for (int igene = 0; igene < num_genes; igene++) {
      if (gene_fitness[igene]<least_fit_gene_fitness ||
	  least_fit_gene==-1) {
	least_fit_gene_fitness = gene_fitness[igene];
	least_fit_gene=igene;
      }
    }
    //printf("checking %i %f %f\n", least_fit_gene, least_fit_gene_fitness, select_fitness[iselect]);
    if (select_fitness[iselect] >= least_fit_gene_fitness && 
	!contains_gene(selects[iselect])) {
      gene_fitness[least_fit_gene] = select_fitness[iselect];
      genes[least_fit_gene]->copy(selects[iselect]);
    }
  }
 printf("-----Done with Picking-----\n");
 printf(" \n");
}

  
      
void pool::mutate(int fnav, int* fav) {
    printf("-----Start Mutation-----");
  for (int iselect = 0; iselect < num_genes; iselect++) {
    //printf("p m %i\n", iselect);
    printf("\nChecking gene %i.", iselect);
    selects[iselect]->print_gene();
    printf("mutation possibility:");
    selects[iselect]->mutate(fnav, fav);
  }
 printf("\n-----Done with Mutation-----\n");
 printf(" \n");
}

int pool::get_calculation_list (gene* gene_in, genelist* db, int num_genes_c) {
  double fitness;
  int calculates_id;
  if (db->find_gene(gene_in, &fitness)) {
    num_genes_c = num_genes_c;
  return num_genes_c;
  }  
  calculates[num_genes_c]->copy(gene_in);
  num_genes_c = num_genes_c + 1;
  return num_genes_c;
}



double pool::check_fitness(gene* gene_in, genelist* db) {
 double fitness;
    if (db->find_gene(gene_in, &fitness)) {
    return fitness;
  }
   printf("Gene ");
   gene_in->print_gene();
   printf(" not in the database, calculation requested.\n");
   return 0.0000;
}  

double pool::calculate_fitness(int gene_id) {
  double fitness;
  char external_name[100]; 
  sprintf(external_name, "./external%i.script",gene_id);
  system(external_name);
  char file_name_result[20];
  sprintf(file_name_result, "result%i.tmp", gene_id);
  FILE* result = fopen(file_name_result,"r");
  fscanf(result, "%lf", &fitness);
  fclose(result); 
  return fitness;
}  

int pool::contains_gene(gene* gene_in) {
  for (int igene=0; igene < num_genes; igene++) {
    if (gene_in->is_equal_to(genes[igene])) return 1;
  }
  return 0;
}

void pool::print_restart(genelist* db) {
  FILE* check = fopen("check.dat", "w");
  fprintf(check," ");
  fclose(check);
  for (int igene = 0; igene < num_genes; igene++) {
  double fitness = check_fitness(genes[igene], db);
  FILE* check = fopen("check.dat", "a+");
  fprintf(check,  "%lf ",fitness);
  fclose(check);
  genes[igene]->print_gene_restart();  
  }
}
