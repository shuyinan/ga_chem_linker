#ifndef _genenode_h_
#define _genenode_h_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "gene.h"

class genenode
{
 public:
  genenode();
  genenode(gene* gene_in, double fit, double mutprob);
  ~genenode();
  genenode* get_next();
  double get_fitness();
  gene* get_gene();
  void set_next(genenode* gene_in);
  int is_less_than(gene* gene_in);
  int is_greater_than(gene* gene_in);
  int is_equal_to(gene* gene_in);

 private:
  gene* data;
  double fitness;
  genenode* next;
};

#endif
