#include "fragment.h"

fragment::fragment() {
  num_atoms=0;
  num_sites=0;
}

fragment::~fragment() {
  return;
}

void fragment::add_atom(char* label, int idist, double dist, int iang, 
			double ang,int idihed, double dihed) {
  labels[num_atoms][0]=label[0];
  labels[num_atoms][1]=label[1];
  idistances[num_atoms]=idist;
  distances[num_atoms]=dist;
  iangles[num_atoms]=iang;
  angles[num_atoms]=ang;
  idihedrals[num_atoms]=idihed;
  dihedrals[num_atoms]=dihed;
  num_atoms++;
}

void fragment::add_site(int isdist, int isang, int isdihed, double sdihed) {
  isite_distances[num_sites]=isdist;
  isite_angles[num_sites]=isang;
  isite_dihedrals[num_sites]=isdihed;
  site_dihedrals[num_sites]=sdihed;
  num_sites++;
}

void fragment::store_allowed_fragments(int isite, int n_allow_frag, 
				    int* i_allow_frag) {
  num_allowed_fragments[isite]=n_allow_frag;
  for (int ifrag=0; ifrag < n_allow_frag; ifrag++) {
    i_allowed_fragments[isite][ifrag] = i_allow_frag[ifrag];
  }
}

int fragment::read_from_file(FILE* input, int ifrag) {
  char junk[5]="";
  char label[2]="";
  int natoms, nsites, nallow, jfrag, jsite;
  int i, j, k;
  double x, y, z;
  int tmp[MAX_ALLOWED_FRAGS];

  fscanf(input, "%s%i", junk, &jfrag);

//  printf("%s%i\n", junk, jfrag);

  if (ifrag!=jfrag) {
    printf("Error!  Fragments are not propertly labeled.\n");
    printf("Fragment labeled %i should be %i\n", jfrag, ifrag);
    return -1;
  }
  
  fscanf(input, "%i%s", &natoms, junk);

//  printf("%i %s\n", natoms, junk);

  for (int iatom=0; iatom<natoms; iatom++) {
    fscanf(input, "%s%i%lf%i%lf%i%lf", label, &i, &x, &j, &y, &k, &z);

//    printf("%s %i %lf %i %lf %i %lf\n", label, i, x, j, y, k, z);

    add_atom(label, i, x, j, y, k, z);
  }

  fscanf(input, "%i%s", &nsites, junk);

//  printf("%i %s\n", nsites, junk);

  for (int isite=0; isite<nsites; isite++) {
    fscanf(input, "%s%i", junk, &jsite);

    if (isite!=jsite) {
      printf("Error!  Sites are not propertly labeled on fragment %i.\n", 
	     ifrag);
      printf("Site labeled %i should be %i\n", jsite, isite);
      return -1;
    }
  
    fscanf(input, "%i%i%i%lf", &i, &j, &k, &z);

//    printf("site %i: fragments link to atom : %i\n", i, i);
//    printf("        fragment-atom%i-atom%i angle: %lf\n", i, j, z);
//    printf("        dihedral angle: fragment-atom%i-atom%i-atom%i\n", i, j, k); 
    
    add_site(i,j,k,z);
   
//    printf("        allowed fragments: ");
 
    fscanf(input, "%i%s", &nallow, junk);
    for (int iallow=0; iallow<nallow; iallow++) {
      fscanf(input, "%i", &(tmp[iallow]));
//      printf("%i ", tmp[iallow]);
    }
//    printf("\n");

    store_allowed_fragments(isite, nallow, tmp);
  }
  return 0;
}
  
int fragment::get_num_atoms() {
  return num_atoms;
}

char* fragment::get_label(int iatom) {
  return &(labels[iatom][0]);
}

int fragment::get_idistance(int iatom) {
  return idistances[iatom];
}

int fragment::get_iangle(int iatom) {
  return iangles[iatom];
}

int fragment::get_idihedral(int iatom) {
  return idihedrals[iatom];
}

int fragment::get_isite_distance(int iatom) {
  return isite_distances[iatom];
}

int fragment::get_isite_angle(int iatom) {
  return isite_angles[iatom];
}

int fragment::get_isite_dihedral(int iatom) {
  return isite_dihedrals[iatom];
}

double fragment::get_distance(int iatom) {
  return distances[iatom];
}

double fragment::get_angle(int iatom) {
  return angles[iatom];
}

double fragment::get_dihedral(int iatom) {
  return dihedrals[iatom];
}

double fragment::get_site_dihedral(int iatom) {
  return site_dihedrals[iatom];
}

int fragment::get_num_sites() {
  return num_sites;
}

int fragment::get_num_allowed(int isite) {
  return  num_allowed_fragments[isite];
}

void fragment::get_allowed_fragments(int isite, int* i_allow_frag) {
  for (int iallow=0; iallow<num_allowed_fragments[isite]; iallow++) {   
    i_allow_frag[iallow] = i_allowed_fragments[isite][iallow];
  }
}
