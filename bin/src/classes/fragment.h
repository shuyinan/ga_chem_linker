#ifndef _fragment_h_
#define _fragment_h_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_ATOMS_IN_FRAG 200
#define MAX_SITES_IN_FRAG 10
#define MAX_ALLOWED_FRAGS 30

class fragment
{
 public:
  fragment();
  ~fragment();
  void add_atom(char* label, int idist, double dist, int iang, double ang,
		int idihed, double dihed);
  void add_site(int isdist, int isang, int isdihed, double sdihed);
  void store_allowed_fragments(int isite, int n_allow_frag, int* i_allow_frag);
  int read_from_file(FILE* input, int ifrag);
  int get_num_atoms();
  char* get_label(int iatom);
  int get_idistance(int iatom);
  int get_iangle(int iatom);
  int get_idihedral(int iatom);
  int get_isite_distance(int iatom);
  int get_isite_angle(int iatom);
  int get_isite_dihedral(int iatom);
  double get_distance(int iatom);
  double get_angle(int iatom);
  double get_dihedral(int iatom);
  double get_site_dihedral(int iatom);
  int get_num_sites();
  int get_num_allowed(int isite);
  void get_allowed_fragments(int isite, int* i_allow_frag);

 private:
  int num_atoms;
  char labels[MAX_ATOMS_IN_FRAG][2];
  double distances[MAX_ATOMS_IN_FRAG];
  double angles[MAX_ATOMS_IN_FRAG];
  double dihedrals[MAX_ATOMS_IN_FRAG];
  int idistances[MAX_ATOMS_IN_FRAG];
  int iangles[MAX_ATOMS_IN_FRAG];
  int idihedrals[MAX_ATOMS_IN_FRAG];
  int num_sites;
  int isite_distances[MAX_SITES_IN_FRAG];
  int isite_angles[MAX_SITES_IN_FRAG];
  int isite_dihedrals[MAX_SITES_IN_FRAG];
  double site_dihedrals[MAX_SITES_IN_FRAG];
  int num_allowed_fragments[MAX_SITES_IN_FRAG];
  int i_allowed_fragments[MAX_SITES_IN_FRAG][MAX_ALLOWED_FRAGS];
};

#endif
