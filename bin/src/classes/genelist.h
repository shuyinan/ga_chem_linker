#ifndef _genelist_h_
#define _genelist_h_

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "genenode.h"
#include "fragment.h"

class genelist
{
 public:
  genelist();
  ~genelist();
  int add(gene* gene_in, double fit);
  int find_gene(gene* gene_in, double* fit_out);
  void write_to_disk();
  void read_from_disk(fragment** frags);
  int get_length();
 private:
  genenode* first;
  int length;
};

#endif
